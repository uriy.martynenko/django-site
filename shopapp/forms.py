from django import forms
from django.contrib.auth.models import Group

from shopapp.models import Order, Product


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = 'user', 'products', 'delivery_address', 'promocode'


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ('name',)


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = 'name', 'price', 'description', 'discount', 'preview'

    images = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
